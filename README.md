# Lib_iOS

[![CI Status](https://img.shields.io/travis/Tuấn Nguyễn/Lib_iOS.svg?style=flat)](https://travis-ci.org/Tuấn Nguyễn/Lib_iOS)
[![Version](https://img.shields.io/cocoapods/v/Lib_iOS.svg?style=flat)](https://cocoapods.org/pods/Lib_iOS)
[![License](https://img.shields.io/cocoapods/l/Lib_iOS.svg?style=flat)](https://cocoapods.org/pods/Lib_iOS)
[![Platform](https://img.shields.io/cocoapods/p/Lib_iOS.svg?style=flat)](https://cocoapods.org/pods/Lib_iOS)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Lib_iOS is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Lib_iOS'
```

## Author

Tuấn Nguyễn, anhtuanqn1002@gmail.com

## License

Lib_iOS is available under the MIT license. See the LICENSE file for more info.
