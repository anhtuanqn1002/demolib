#
# Be sure to run `pod lib lint Lib_iOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Lib_iOS'
  s.version          = '0.1.0'
  s.summary          = 'Lib iOS: demo for cocoapods lib'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
Hello Lib iOS
                       DESC

  s.homepage         = 'https://anhtuanqn1002@bitbucket.org/anhtuanqn1002/demolib'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tuấn Nguyễn' => 'anhtuanqn1002@gmail.com' }
  s.source           = { :git => 'https://anhtuanqn1002@bitbucket.org/anhtuanqn1002/demolib.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_version = '4.2'
  s.source_files = 'Lib_iOS/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Lib_iOS' => ['Lib_iOS/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
